﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Diploma
{
    public class Shop : MonoBehaviour
    {
        [SerializeField]
        private List<Plane> _products;
        [SerializeField]
        private Button _closeButton;
        private CanvasGroup _currCanvasGroup;

        private void OnEnable()
        {
            ProductsStartAnim();
        }

        public void UseSettings(List<DataPlanes> dataPlanes, List<int> boughtPlanes, int currentPlane)
        {
            for (int i = 0; i < dataPlanes.Count; i++)
            {
                bool isBought = false;
                bool isCurrentPlane = false;
                foreach (var item in boughtPlanes)
                {
                    if (dataPlanes[i].id == item)
                    {
                        isBought = true;
                    }
                    if (dataPlanes[i].id == currentPlane)
                    {
                        isCurrentPlane = true;
                    }
                }
                _products[i].UseSetings(dataPlanes[i].id, dataPlanes[i].price, isBought, isCurrentPlane);
            }
        }

        public void RefreshSelected()
        {
            foreach(var item in _products)
            {
                item.ActiveSelectButton();
            }
        }

        private void Start()
        {
            _currCanvasGroup = gameObject.GetComponent<CanvasGroup>();
            AddListeners();
        }

        private void AddListeners()
        {
            _closeButton.onClick.AddListener(CloseClick);
        }

        private void CloseClick()
        {
            CloseShopAnim();
        }

        private void CloseShopAnim()
        {
            LeanTween.value(gameObject, _currCanvasGroup.alpha, 0f, 0.3f).setOnUpdate((float val) =>
            {
                _currCanvasGroup.alpha = val;
            }).setOnComplete(() =>
            {
                gameObject.SetActive(false);
            });
        }

        private void ProductsStartAnim()
        {
            float del = 0;
            foreach (var item in _products)
            {
                LeanTween.scale(item.gameObject, new Vector2(0.2f, 0.2f), 0f);
                LeanTween.alpha(item.GetComponent<RectTransform>(), 0f, 0f);
            }
            foreach (var item in _products)
            {
                LeanTween.delayedCall(gameObject, del, () =>
                {
                    ProductEnableAnim(item.gameObject);
                });
                del += 0.2f;
            }
        }

        private void ProductEnableAnim(GameObject product)
        {
            LeanTween.scale(product, new Vector2(0.6f, 0.6f), 0.15f);
            LeanTween.alpha(product.GetComponent<RectTransform>(), 1, 0.15f);
            LeanTween.rotateZ(product, -8, 0.1f).setOnComplete(() =>
            {
                LeanTween.rotateZ(product, 0, 0.1f).setOnComplete(() =>
                {
                    LeanTween.rotateZ(product, 7, 0.1f).setOnComplete(() =>
                    {
                        LeanTween.rotateZ(product, 0, 0.1f).setOnComplete(() =>
                        {
                            LeanTween.rotateZ(product, -5, 0.1f).setOnComplete(() =>
                            {
                                LeanTween.rotateZ(product, 0, 0.1f).setOnComplete(() =>
                                {
                                    LeanTween.rotateZ(product, 3, 0.1f).setOnComplete(() =>
                                    {
                                        LeanTween.rotateZ(product, 0, 0.1f).setOnComplete(() =>
                                        {
                                            LeanTween.rotateZ(product, -1, 0.1f).setOnComplete(() =>
                                            {
                                                LeanTween.rotateZ(product, 0, 0.1f);
                                            });
                                        });
                                    });
                                });
                            });
                        });
                    });
                });
            });
        }
    }
}
