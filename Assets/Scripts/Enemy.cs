﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Diploma
{
    public class Enemy : MonoBehaviour
    {
        #region Constants
        private const string _DESTOYED_TRIGER = "IsDestroyed";
        private const string _BULLET_TAG = "bullet";
        private const string _PLAYER_TAG = "Player";
        #endregion

        #region CachingData
        private Animator _animator;
        private Sprite _enemySprite;
        private SpriteRenderer _currSpriteRenderer;
        private Collider2D _currCollider;
        #endregion

        [SerializeField]
        private List<GameObject> _bullets;
        [SerializeField]
        private List<Transform> _bulletParents;
        [SerializeField]
        private int _reward;
        [SerializeField]
        private int _powerBullet = 100;

        private GameObject _bullet;
        private Vector2 _moveTo;
        private PoolObjects _poolObjects;
        private Coroutine _currCoroutine;
        private AudioSource _currAudioSource;

        private float _speed;
        private int _health;

        private void Start()
        {
            AddListeners();
        }

        public void EnemyDestroy()
        {
            _currCollider.enabled = false;
            _animator.SetTrigger(_DESTOYED_TRIGER);
            _currAudioSource.Play();
            LeanTween.cancel(gameObject);
            LeanTween.delayedCall(gameObject, 1f, () =>
            {
                gameObject.SetActive(false);
            });
        }

        public void UseSettings(Vector2 moveToPos, int type)
        {
            CachingData();
            _currSpriteRenderer.sprite = _enemySprite;
            _currCollider.enabled = true;
            if (_bullet == null)
            {
                DefineType(type);
            }
            if (_poolObjects == null)
            {
                List<GameObject> _bulletsTmp = new List<GameObject>();
                _bulletsTmp.Add(_bullet);
                _poolObjects = new PoolObjects(_bulletsTmp);
            }
            _currCoroutine = StartCoroutine(Shots());
            _moveTo = moveToPos;
            Move();
        }

        private void AddListeners()
        {
            EventManager.lostEvent += LostEventFunc;
        }

        private void Move()
        {
            _speed = 10;
            LeanTween.move(gameObject, _moveTo, _speed).setOnComplete(() =>
            {
                gameObject.SetActive(false);
            });
        }

        private void LostEventFunc()
        {
            gameObject.SetActive(false);
        }

        private void CachingData()
        {
            if (_currSpriteRenderer == null)
            {
                _currSpriteRenderer = gameObject.GetComponent<SpriteRenderer>();
                _enemySprite = _currSpriteRenderer.sprite;
            }
            if (_animator == null)
            {
                _animator = gameObject.GetComponent<Animator>();
            }
            if (_currCollider == null)
            {
                _currCollider = gameObject.GetComponent<Collider2D>();
            }
            if(_currAudioSource==null)
            {
                _currAudioSource = gameObject.GetComponent<AudioSource>();
            }
        }

        private void DefineType(int type)
        {
            _bullet = _bullets[0];
            switch (type)
            {
                case 0:
                    _health = 10;
                    break;
                case 1:
                    _health = 20;
                    break;
                case 2:
                    _health = 30;
                    break;
                case 3:
                    _health = 40;
                    break;
            }
        }

        private IEnumerator Shots()
        {
            while (true)
            {
                foreach (var item in _bulletParents)
                {
                    var bull = _poolObjects.GetObject(item.position, _bullet.tag);
                    bull.GetComponent<Transform>().transform.rotation = new Quaternion(0, 0, 180f, 0);
                    Vector2 pos = bull.transform.position;
                    pos.x -= _powerBullet;
                    bull.GetComponent<Rigidbody2D>().AddForce(new Vector2(-500, 0));
                }
                yield return new WaitForSeconds(0.8f);
            }
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.gameObject.tag == _BULLET_TAG)
            {
                var bullet = collision.GetComponent<Bullet>();
                collision.gameObject.SetActive(false);
                _health -= bullet.GetDamage();
                if (_health <= 0)
                {
                    EventManager.destroyedEnemy?.Invoke(_reward);
                    EnemyDestroy();
                }
            }
            if (collision.gameObject.tag == _PLAYER_TAG)
            {
                _health = 0;
                EnemyDestroy();
            }
        }

        private void OnCollisionEnter2D(Collision2D collision)
        {
            if (collision.gameObject.tag == _PLAYER_TAG)
            {
                _health = 0;
                EnemyDestroy();
            }
        }

        private void OnDisable()
        {
            StopCoroutine(_currCoroutine);
            LeanTween.cancel(gameObject);
        }

        private void OnDestroy()
        {
            EventManager.lostEvent -= LostEventFunc;
        }
    }
}