﻿using UnityEngine;
using UnityEngine.UI;

public class EndGame : MonoBehaviour
{
    [SerializeField]
    private Text _textScore;
    [SerializeField]
    private Text _rewardText;
    [SerializeField]
    private GameObject _newRecordText;
    [SerializeField]
    private GameObject _effectsNewRecord;
    
    public void UseSettings(string score,string reward, bool isNewRecord)
    {
        _textScore.text = score;
        _rewardText.text = reward;
        _newRecordText.SetActive(isNewRecord);
        _effectsNewRecord.SetActive(isNewRecord);
    }
}