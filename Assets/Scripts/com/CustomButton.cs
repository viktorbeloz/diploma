﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class CustomButton : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerClickHandler
{
    public class MuDoubleClick : UnityEvent<int> { };

    public UnityEvent onClickUp;
    public UnityEvent onClickDown;
    public UnityEvent onDoubleClick;

    public void OnPointerDown(PointerEventData eventData)
    {
        onClickDown.Invoke();
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        onClickUp.Invoke();
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (eventData.clickCount == 2)
        {
            onDoubleClick.Invoke();
            eventData.clickCount = 0;
        }
    }
}
