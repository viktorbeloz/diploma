﻿using System;
using System.Collections.Generic;

namespace Diploma
{
    [Serializable]
    public class DataPlayer
    {
        public int gold;
        public string crystals;
        public int record;
        public int currentPlane;
        public int currentBullet;
        public int[] goldForUpgrades;
        public List<int> boughtPlanes;
        public int typeBullet;
        public List<DataPlanes> allPlanes;
    }

    [Serializable]
    public class DataPlanes
    {
        public int id;
        public int price;
    }
}