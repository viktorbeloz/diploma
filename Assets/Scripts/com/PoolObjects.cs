﻿using System.Collections.Generic;
using UnityEngine;

public class PoolObjects : MonoBehaviour
{
    private List<GameObject> _prefabs;

    private int count = 0;
    private List<GameObject> _objects = new List<GameObject>();

    public PoolObjects(List<GameObject> prefabs)
    {
        _prefabs = prefabs;
    }

    public GameObject GetObject(Vector2 _parent, string tag)
    {
        GameObject obj;

        if (_objects == null)
        {
            obj = Instantiate(GetPrefabOrTag(tag),_parent, Quaternion.identity);
            _objects.Add(obj);
            return obj;
        }
        else
        {
            foreach (var item in _objects)
            {
                if (!item.activeInHierarchy&&item.CompareTag(tag))
                {
                    item.GetComponent<Transform>().transform.position = _parent;
                    item.SetActive(true);
                    return item;
                }
            }
        }
        obj = Instantiate(GetPrefabOrTag(tag), _parent,Quaternion.identity);
        _objects.Add(obj);
        return obj;
    }

    private GameObject GetPrefabOrTag(string tag)
    {
        foreach (var item in _prefabs)
        {
            if (item.CompareTag(tag))
            {
                return(item);
            }
        }
        return null;
    }

    public void Clear()
    {
        foreach (var item in _objects)
        {
            Destroy(item);
        }
        _objects.Clear();
    }
}
