﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Diploma
{
    public class Player : MonoBehaviour
    {
        private const string _ENEMY_TAG = "enemy";
        private const string _ENEMY_BULLET_TAG = "enemyBullet";
        private const string _HEAL_TAG = "heal";

        [SerializeField]
        private float _speed;
        [SerializeField]
        private float _speedTurn;
        [SerializeField]
        private int _maxHealth;

        private GameObject _bulletPrefab;
        [SerializeField]
        private GameObject _turbo;
        [SerializeField]
        private Transform _bulletParent;

        public Animator animator { get; set; }
        private Rigidbody2D _playerRb;
        private PoolObjects _poolObjects;
        private int _health;

        private void Awake()
        {
            _health = _maxHealth;
        }

        private void Start()
        {
            AddListeners();
            InitData();
            StartCoroutine(Shots());
            CachingData();
        }

        public void SetBullet(GameObject _bullet)
        {
            _bulletPrefab = _bullet;
        }

        public int GetHP()
        {
            return _health;
        }

        public void MoveLeft()
        {
            _playerRb.AddForce(new Vector2(-_speedTurn, _speed));
        }

        public void MoveRight()
        {
            _playerRb.AddForce(new Vector2(_speedTurn, _speed));
        }

        public void TurboIsEnable(bool isEnable)
        {
            _turbo.SetActive(isEnable);
        }

        private void InitData()
        {
            List<GameObject> tmpPrefabs = new List<GameObject>();
            tmpPrefabs.Add(_bulletPrefab);
            _poolObjects = new PoolObjects(tmpPrefabs);
        }

        private void AddListeners()
        {
            EventManager.lostEvent += LostEventFunc;
        }

        private void CachingData()
        {
            animator = gameObject.GetComponent<Animator>();
            _playerRb = gameObject.GetComponent<Rigidbody2D>();
        }

        private void LostEventFunc()
        {
            gameObject.SetActive(false);
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.gameObject.tag == _ENEMY_TAG)
            {
                collision.gameObject.GetComponent<Enemy>().EnemyDestroy();
                _health -= 10;
                CheckHealth();
            }
            if (collision.gameObject.tag == _ENEMY_BULLET_TAG)
            {
                int damage = collision.gameObject.GetComponent<Bullet>().GetDamage();
                collision.gameObject.GetComponent<Bullet>().DestroyAnim();
                _health -= damage;
                CheckHealth();
            }
            if (collision.gameObject.tag == _HEAL_TAG)
            {
                collision.gameObject.SetActive(false);
                _health += 20;
                if (_health > _maxHealth)
                {
                    _health = _maxHealth;
                }
                CheckHealth();
            }
        }

        private void CheckHealth()
        {
            if (_health <= 0)
            {
                EventManager.lostEvent?.Invoke();
            }
            else
            {
                EventManager.changeHealth?.Invoke();
            }
        }

        private IEnumerator Shots()
        {
            while (true)
            {
                gameObject.GetComponent<AudioSource>().Play();
                GameObject bull = _poolObjects.GetObject(_bulletParent.position, _bulletPrefab.tag);
                bull.GetComponent<Rigidbody2D>().AddForce(new Vector2(500,0));
                yield return new WaitForSeconds(0.2f);
            }
        }

        private void OnDestroy()
        {
            EventManager.lostEvent -= LostEventFunc;
        }
    }
}