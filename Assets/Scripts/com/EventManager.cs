﻿namespace Diploma
{

    public  static class EventManager
    {
        public delegate void LostEvent();
        public delegate void ChangeHealth();
        public delegate void BoughtPlane(int id);
        public delegate void SelectPlane(int id);
        public delegate void SuccesssBought(int id);
        public delegate void SuccesssSelected(int id);
        public delegate void DestroyedEnemy(int reward);

        public static LostEvent lostEvent;
        public static ChangeHealth changeHealth;
        public static BoughtPlane boughtPlane;
        public static SelectPlane selectPlane;
        public static SuccesssBought successsBought;
        public static SuccesssSelected successsSelected;
        public static DestroyedEnemy destroyedEnemy;
    }
}