﻿using UnityEngine;

namespace Diploma
{
    public class Bullet : MonoBehaviour
    {
        private const string _END_BULLET_TAG = "endBullet";

        [SerializeField]
        private int _damage;

        public int GetDamage()
        {
            return _damage;
        }

        public void DestroyAnim()
        {
            gameObject.SetActive(false);
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.gameObject.tag == _END_BULLET_TAG)
            {
                gameObject.SetActive(false);
            }
        }
    }
}