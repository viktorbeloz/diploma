﻿using System.Collections.Generic;
using UnityEngine;

namespace Diploma
{
    public static class DatabaseManager
    {
        private const string _IS_FIRST_ENT = "IsFirstEnt";
        private const string _PLAYER_DATA = "PlayerData";

        private static DataPlayer _dataPlayer = new DataPlayer();

        public static void SaveData()
        {
            string json = JsonUtility.ToJson(_dataPlayer);
            PlayerPrefs.SetString(_PLAYER_DATA, json);
        }

        public static void LoadData()
        {
            if (PlayerPrefs.GetInt(_IS_FIRST_ENT) == 0)
            {
                StarterData();
                SaveData();
                PlayerPrefs.SetInt(_IS_FIRST_ENT, 1);
            }
            string json = PlayerPrefs.GetString(_PLAYER_DATA);
            _dataPlayer = JsonUtility.FromJson<DataPlayer>(json);
        }

        public static DataPlayer GetData()
        {
            return _dataPlayer;
        }

        /// <summary>Default data</summary>
        private static void StarterData()
        {
            List<DataPlanes> dataPlanes = new List<DataPlanes>
            {
                new DataPlanes{id = 0, price = 1000},
                new DataPlanes{id = 1, price = 5000},
                new DataPlanes{id = 2, price = 50000},
                new DataPlanes{id = 3, price = 100000},
                new DataPlanes{id = 4, price = 150000},
                new DataPlanes{id = 5, price = 175000},
                new DataPlanes{id = 6, price = 200000},
                new DataPlanes{id = 7, price = 210000},
                new DataPlanes{id = 8, price = 275000},
                new DataPlanes{id = 9, price = 300000}
            };

            _dataPlayer = new DataPlayer
            {
                gold = 1000000,
                crystals = "50",
                record = 0,
                currentPlane = 0,
                currentBullet = 0,
                goldForUpgrades = new int[] {10000,12000,17000,22000,30000, 34000, 40000 },
                boughtPlanes = new List<int> { 0 },
                allPlanes = dataPlanes,
                typeBullet = 0
            };
        }
    }
}