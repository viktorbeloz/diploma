﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


namespace Diploma
{
    public enum TypeEnemy
    {
        TOP = 0,
        BOTTOM = 1,
        RIGHT = 2,
        RADIO = 3
    }

    public enum TypePlanePlayer
    {
        Level1 = 0,
        Level2 = 1,
        Level3 = 2,
    }

    public class GameController : MonoBehaviour
    {
        [SerializeField]
        private RectTransform _leftPointHealBox;
        [SerializeField]
        private RectTransform _rightPointHealBox;


        #region PointsSpawning
        [SerializeField]
        private RectTransform _leftTop;
        [SerializeField]
        private RectTransform _rightTop;
        [SerializeField]
        private RectTransform _leftBottom;
        [SerializeField]
        private RectTransform _rightBottom;
        #endregion
        #region PointsMoving
        [SerializeField]
        private RectTransform _moveToTop;
        [SerializeField]
        private RectTransform _moveToBottom;
        #endregion

        [SerializeField]
        private CanvasGroup _endGamePanel;
        [SerializeField]
        private CanvasGroup _backgroundLight;
        [SerializeField]
        private CanvasGroup _backgroundDark;
        private CanvasGroup _currentBackground;

        [SerializeField]
        private List<GameObject> _enemyPrefabs;
        [SerializeField]
        private List<GameObject> _bulletPrefabs;
        [SerializeField]
        private List<GameObject> _players;

        [SerializeField]
        private Transform _enemyInstParent;
        [SerializeField]
        private Transform _spawnPlayer;

        [SerializeField]
        private CustomButton _leftGasButton;
        [SerializeField]
        private CustomButton _rightGasButton;
        [SerializeField]
        private Button _lostPanelReplay;
        [SerializeField]
        private Button _lostPanelExit;

        [SerializeField]
        private Text _scoreText;
        [SerializeField]
        private Text _health;

        [SerializeField]
        private GameObject _healBox;

        private PoolObjects poolObjects;
        private Player _player;
        private DataPlayer _dataPlayer;

        private bool isGasLeft;
        private bool isGasRight;
        private bool isActive;
        private int _scoreCounter;
        private int _rewardCounter;

        private bool _isLost;
        private int _enemiesMaxType;
        private float _timeForCreateEnemy;
        private float _timeForCreateHealBox;

        private void Awake()
        {
            DatabaseManager.LoadData();
            _dataPlayer = DatabaseManager.GetData();
        }

        private void Start()
        {
            _enemiesMaxType = 1;
            _timeForCreateEnemy = 4;
            _timeForCreateHealBox = 8;
            poolObjects = new PoolObjects(GetAllPrefabs());
            _isLost = false;
            DefinePlayerPlane(_dataPlayer.currentPlane);
            _currentBackground = _backgroundDark;
            ChangeHealth();
            AddListeners();
            StartCoroutines();
        }

        private List<GameObject> GetAllPrefabs()
        {
            List<GameObject> prefabs = new List<GameObject>();
            prefabs.AddRange(_enemyPrefabs);
            prefabs.Add(_bulletPrefabs[0]);
            prefabs.Add(_healBox);
            return prefabs;
        }

        private void StartCoroutines()
        {
            StartCoroutine(ChangeBackTimer());
            StartCoroutine(GenerateEnemiesRandom());
            StartCoroutine(ScoreCounter());
            StartCoroutine(GenerateHealBoxes());
        }

        private void AddListeners()
        {
            _leftGasButton.onClickUp.AddListener(LeftGasClickUp);
            _leftGasButton.onClickDown.AddListener(LeftGasClickDown);
            _rightGasButton.onClickUp.AddListener(RightGasClickUp);
            _rightGasButton.onClickDown.AddListener(RightGasClickDown);
            _lostPanelExit.onClick.AddListener(LostPanelExit);
            _lostPanelReplay.onClick.AddListener(LostPanelReplay);
            EventManager.lostEvent += LostEventFunc;
            EventManager.changeHealth += ChangeHealth;
            EventManager.destroyedEnemy += DestroyedEnemyFunc;
        }

        private void DestroyedEnemyFunc(int reward)
        {
            _rewardCounter += reward;
        }

        private void DefinePlayerPlane(int currPlaneType)
        {
            var pl = Instantiate(_players[currPlaneType], _spawnPlayer.position, Quaternion.identity);
            _player = pl.GetComponent<Player>();
            _player.SetBullet(_bulletPrefabs[_dataPlayer.currentBullet]);
        }

        private void LostEventFunc()
        {
            _isLost = true;
            StopAllCoroutines();
            OpenEndGamePanel();
        }

        private void LostPanelExit()
        {
            SceneManager.LoadScene("Menu");
        }

        private void LostPanelReplay()
        {
            SceneManager.LoadScene("Lvl1");
        }

        private void FixedUpdate()
        {
            if (isGasLeft)
            {
                _player.MoveLeft();
                _player.TurboIsEnable(true);
            }
            if (isGasRight)
            {
                _player.MoveRight();
                _player.TurboIsEnable(true);
            }
            if (!isGasLeft && !isGasRight)
            {
                _player.TurboIsEnable(false);
            }
        }

        #region PlayerController
        private void LeftGasClickUp()
        {
            isGasLeft = false;
        }

        private void LeftGasClickDown()
        {
            isGasRight = false;
            isGasLeft = true;
        }

        private void RightGasClickUp()
        {
            isGasRight = false;
        }

        private void RightGasClickDown()
        {
            isGasLeft = false;
            isGasRight = true;
        }
        #endregion

        #region GenerationFlyEnemy
        private IEnumerator GenerateEnemiesRandom()
        {
            while (!_isLost)
            {
                int rnd = Random.Range(0, 3);
                CreateEnemy(rnd);
                yield return new WaitForSeconds(_timeForCreateEnemy);
            }
        }
        #endregion

        private void ChangeHealth()
        {
            _health.text = _player.GetHP().ToString();
        }

        private void CreateEnemy(int typePos)
        {
            Vector2 instPos = new Vector2();
            Enemy enemy = null;
            GameObject enemyPrefab = _enemyPrefabs[Random.Range(0, _enemiesMaxType)];
            switch (typePos)
            {
                case 0:
                    instPos = new Vector2(Random.Range(_leftTop.position.x, _rightTop.position.x), _rightTop.position.y);
                    var enemy1 = poolObjects.GetObject(instPos, enemyPrefab.tag);
                    enemy = enemy1.GetComponent<Enemy>();
                    enemy.UseSettings(_moveToBottom.position, typePos);
                    break;
                case 1:
                    instPos = new Vector2(Random.Range(_leftTop.position.x, _rightTop.position.x), _rightBottom.position.y);
                    var enemy2 = poolObjects.GetObject(instPos, enemyPrefab.tag);
                    enemy = enemy2.GetComponent<Enemy>();
                    enemy.UseSettings(_moveToTop.position, typePos);
                    break;
                case 2:
                    instPos = new Vector2(_rightTop.position.x, Random.Range(_rightTop.position.y, _rightBottom.position.y));
                    var enemy3 = poolObjects.GetObject(instPos, enemyPrefab.tag);
                    enemy = enemy3.GetComponent<Enemy>();
                    Vector2 movePos = new Vector2(_moveToTop.position.x, Random.Range(_moveToTop.position.y, _moveToBottom.position.y));
                    enemy.UseSettings(movePos, typePos);
                    break;
            }
        }

        private void OpenEndGamePanel()
        {
            _dataPlayer.gold += _rewardCounter;
            bool isNewRecord = false;
            if(_scoreCounter> _dataPlayer.record)
            {
                isNewRecord = true;
                _dataPlayer.record = _scoreCounter;
            }
            DatabaseManager.SaveData();
            _endGamePanel.gameObject.SetActive(true);
            _endGamePanel.GetComponent<EndGame>().UseSettings(_scoreCounter.ToString(),_rewardCounter.ToString(),isNewRecord);
            LeanTween.value(_endGamePanel.gameObject,_endGamePanel.alpha,1f,0.2f).setOnUpdate((float val)=> {
                _endGamePanel.alpha = val;
            });
        }

        private void ClosedEndGamePanel()
        {
            _endGamePanel.gameObject.SetActive(false);
            _endGamePanel.alpha = 0;
        }

        private IEnumerator ChangeBackTimer()
        {
            while (!_isLost)
            {
                yield return new WaitForSeconds(10);
                ChangeBackgroundAnim();
            }
        }

        private IEnumerator GenerateHealBoxes()
        { 
            while (!_isLost)
            {
                yield return new WaitForSeconds(_timeForCreateHealBox);
                CreateHealBox();
            }
        }

        private void CreateHealBox()
        {
            Vector2 pos = new Vector2(Random.Range(_leftPointHealBox.position.x,_rightPointHealBox.position.x),_leftPointHealBox.position.y);
            var healBox = poolObjects.GetObject(pos,_healBox.tag);
            LeanTween.moveY(healBox, _rightPointHealBox.position.y-20,4f).setOnComplete(()=> {
                healBox.SetActive(false);
            });
        }

        private IEnumerator ScoreCounter()
        {
            while (!_isLost)
            {
                _scoreCounter++;
                _scoreText.text = $"score: {_scoreCounter}";
                UpdateEnemy();
                yield return new WaitForSeconds(0.05f);
            }
        }

        private void UpdateEnemy()
        {
            if (_scoreCounter % 500 == 0)
            {
                _enemiesMaxType++;
                if (_enemiesMaxType > _enemyPrefabs.Count)
                {
                    _enemiesMaxType = _enemyPrefabs.Count;
                }
            }
            if (_scoreCounter % 100 == 0)
            {
                _timeForCreateEnemy -= 0.15f;
                _timeForCreateHealBox -= 0.15f;
                if (_timeForCreateEnemy < 1)
                {
                    _timeForCreateEnemy = 1;
                }
            }
        }
        private void ChangeBackgroundAnim()
        {
            CanvasGroup _newBack = null;
            if (_currentBackground == _backgroundDark)
            {
                _newBack = _backgroundLight;
            }
            else if (_currentBackground == _backgroundLight)
            {
                _newBack = _backgroundDark;
            }

            LeanTween.value(_currentBackground.gameObject, _currentBackground.alpha, 0, 0.8f).setOnUpdate((float val) =>
            {
                _currentBackground.alpha = val;
            });
            LeanTween.value(_newBack.gameObject, _newBack.alpha, 1, 0.8f).setOnUpdate((float val) =>
            {
                _newBack.alpha = val;
            }).setOnComplete(() =>
            {
                _currentBackground = _newBack;
            });
        }

        private void OnDestroy()
        {
            EventManager.lostEvent -= LostEventFunc;
            EventManager.changeHealth -= ChangeHealth;
        }
    }
}