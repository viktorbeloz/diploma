﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Diploma
{
    public class MenuController : MonoBehaviour
    {
        private const string _IS_FIRST_ENT = "IsFirstEnt";
        private const string _PLAYER_DATA = "PlayerData";
        private const string _GAME_SCENE = "Game"; 

        [SerializeField]
        private List<Sprite> _bulletsSprites;
        [SerializeField]
        private List<GameObject> _planePrefab;
        [SerializeField]
        private List<GameObject> _buttons;

        [SerializeField]
        private Image _bulletImage;
        [SerializeField]
        private Text _priceText;
        [SerializeField]
        private Text _upgradeBulletPrice;

        [SerializeField]
        private CanvasGroup _shopPanel;
        [SerializeField]
        private CanvasGroup _ratingPanel;
        private Shop _shopPanelCaсh;
        private Rating _ratingPanelCaсh;

        [SerializeField]
        private Button _upgradeBulletButton;
        [SerializeField]
        private Button _shopButton;
        [SerializeField]
        private Button _startButton;
        [SerializeField]
        private Button _ratingButton;

        [SerializeField]
        private List<GameObject> _upgradeImgs;

        private DataPlayer _dataPlayer = new DataPlayer();

        private void Start()
        {
            DatabaseManager.LoadData();
            _dataPlayer = DatabaseManager.GetData();
            AddListeners();
            StartButtonAnim();
            StartCoroutine(ButtonsAnimCoroutine());
            SetMenuPlanes(_dataPlayer.currentPlane);
            RefreshGold();
            RefreshBulletSprite();
            CheckBulletUpgrade();
            СachingData();
        }

        private void AddListeners()
        {
            _startButton.onClick.AddListener(StartClick);
            _shopButton.onClick.AddListener(ShopClick);
            _ratingButton.onClick.AddListener(RatingClick);
            _upgradeBulletButton.onClick.AddListener(UpgradeBulletClick);
            EventManager.boughtPlane += BoughtPlaneFunc;
            EventManager.selectPlane += SelectPlaneFunc;
        }

        private void StartButtonAnim()
        {
            LeanTween.scale(_startButton.gameObject, new Vector2(0.95f, 0.95f), 0.7f).setLoopPingPong();
        }

        private void SetMenuPlanes(int id)
        {
            foreach (var item in _planePrefab)
            {
                item.SetActive(false);
            }
            _planePrefab[id].SetActive(true);
        }

        private void RefreshGold()
        {
            _priceText.text = _dataPlayer.gold.ToString();
        }

        private void RefreshBulletSprite()
        {
            _bulletImage.sprite = _bulletsSprites[_dataPlayer.currentBullet];
            if (_dataPlayer.currentBullet < _dataPlayer.goldForUpgrades.Length - 1)
            {
                _upgradeBulletPrice.text = _dataPlayer.goldForUpgrades[_dataPlayer.currentBullet + 1].ToString();
                foreach(var item in _upgradeImgs)
                {
                    item.SetActive(true);
                }
            }
            else
            {
                foreach (var item in _upgradeImgs)
                {
                    item.SetActive(false);
                }
                _upgradeBulletPrice.text = "MAX";
            }
        }

        private void CheckBulletUpgrade()
        {
            int id = _dataPlayer.currentBullet;
            if (_dataPlayer.gold >= _dataPlayer.goldForUpgrades[id] && _dataPlayer.currentBullet < _dataPlayer.goldForUpgrades.Length - 1)
            {
                _upgradeBulletButton.interactable = true;
                BulletUpgradeAnim();
            }
            else
            {
                BulletUpgradeClose();
                _upgradeBulletButton.interactable = false;
            }
        }

        private void СachingData()
        {
            _ratingPanelCaсh = _ratingPanel.GetComponent<Rating>();
            _shopPanelCaсh = _shopPanel.GetComponent<Shop>();
        }

        private void UpgradeBulletClick()
        {
            _dataPlayer.gold -= _dataPlayer.goldForUpgrades[_dataPlayer.currentBullet+1];
            _dataPlayer.currentBullet++;
            if(_dataPlayer.currentBullet < _dataPlayer.goldForUpgrades.Length - 1)
            {
                _upgradeBulletPrice.text = _dataPlayer.goldForUpgrades[_dataPlayer.currentBullet + 1].ToString();
                foreach (var item in _upgradeImgs)
                {
                    item.SetActive(true);
                }
            }
            else
            {
                _upgradeBulletPrice.text = "MAX";
                foreach (var item in _upgradeImgs)
                {
                    item.SetActive(false);
                }
            }
            DatabaseManager.SaveData();
            RefreshGold();
            RefreshBulletSprite();
            CheckBulletUpgrade();
        }

        private void BulletUpgradeAnim()
        {
            LeanTween.scale(_upgradeBulletButton.gameObject, new Vector2(0.95f, 0.95f), 0.7f).setLoopPingPong();
        }

        private void BulletUpgradeClose()
        {
            LeanTween.cancel(_upgradeBulletButton.gameObject);
            LeanTween.scale(_upgradeBulletButton.gameObject, Vector2.one, 0.2f);
        }

        private void BoughtPlaneFunc(int id)
        {
            DataPlanes dataPlanes = null;
            foreach(var item in _dataPlayer.allPlanes)
            {
                if(item.id==id)
                {
                    dataPlanes = item;
                    break;
                }
            }

            if(_dataPlayer.gold - dataPlanes.price>=0)
            {
                _dataPlayer.boughtPlanes.Add(dataPlanes.id);
                _dataPlayer.gold -=dataPlanes.price;
                DatabaseManager.SaveData();
                CheckBulletUpgrade();
                EventManager.successsBought?.Invoke(dataPlanes.id);
                _priceText.text = _dataPlayer.gold.ToString();
            }
        }

        private void SelectPlaneFunc(int id)
        {
            _shopPanelCaсh.RefreshSelected();
            _dataPlayer.currentPlane = id;
            SetMenuPlanes(_dataPlayer.currentPlane);
            EventManager.successsSelected?.Invoke(id);
            DatabaseManager.SaveData();
        }

        private void StartClick()
        {
            SceneManager.LoadScene(_GAME_SCENE);
        }

        private void ShopClick()
        {
            OpenShop();
        }

        private void ButtonsAnim()
        {
            float del = 0;
            foreach (var item in _buttons)
            {
                LeanTween.delayedCall(gameObject, del, () =>
                {
                    LeanTween.rotateZ(item, -5, 0.05f).setOnComplete(() =>
                    {
                        LeanTween.rotateZ(item, 0, 0.05f).setOnComplete(() =>
                        {
                            LeanTween.rotateZ(item, 5, 0.05f).setOnComplete(() =>
                            {
                                LeanTween.rotateZ(item, 0, 0.05f);
                            });
                        });
                    });
                });
                del += 0.2f;
            }
        }

        private void OpenShop()
        {
            _shopPanelCaсh.UseSettings(_dataPlayer.allPlanes,_dataPlayer.boughtPlanes, _dataPlayer.currentPlane);
            CanvasOpenAnim(_shopPanel);
        }

        private void RatingClick()
        {
            _ratingPanelCaсh.UseSettings(_dataPlayer.record.ToString());
            CanvasOpenAnim(_ratingPanel);
        }

        private void CanvasOpenAnim(CanvasGroup panel)
        {
            panel.gameObject.SetActive(true);
            LeanTween.value(panel.gameObject, panel.alpha, 1f, 0.3f).setOnUpdate((float val) =>
            {
                panel.alpha = val;
            });
        }

        private IEnumerator ButtonsAnimCoroutine()
        {
            while (true)
            {
                ButtonsAnim();
                yield return new WaitForSeconds(5f);
            }
        }

        private void OnDestroy()
        {
            EventManager.boughtPlane -= BoughtPlaneFunc;
            EventManager.selectPlane -= SelectPlaneFunc;
        }
    }
}