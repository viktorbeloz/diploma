﻿using UnityEngine;
using UnityEngine.UI;

public class Rating : MonoBehaviour
{
    [SerializeField]
    private Text _scoreText;
    [SerializeField]
    private Button _closeButton;

    private CanvasGroup _currCanvasGroup;

    private void Start()
    {
        AddListeners();
        CachingData();
    }

    public void UseSettings(string score)
    {
        _scoreText.text = score;
    }

    private void CachingData()
    {
        _currCanvasGroup = gameObject.GetComponent<CanvasGroup>();
    }

    private void  AddListeners()
    {
        _closeButton.onClick.AddListener(CloseClick);
    }

    private void CloseClick()
    {
        CloseRatingAnim();
    }

    private void CloseRatingAnim()
    {
        LeanTween.value(gameObject, _currCanvasGroup.alpha, 0f, 0.3f).setOnUpdate((float val) => {
            _currCanvasGroup.alpha = val;
        }).setOnComplete(() => {
            gameObject.SetActive(false);
        });
    }
}