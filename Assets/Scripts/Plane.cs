﻿using UnityEngine;
using UnityEngine.UI;

namespace Diploma
{
    public class Plane : MonoBehaviour
    {
        [SerializeField]
        private Text _priceText;
        [SerializeField]
        private Button _boughtButton;
        [SerializeField]
        private Button _sellectButton;

        private int _id;
        private int _price;
        private bool _isBought;
        private bool _isSelected;

        private void Start()
        {
            AddListeners();
        }

        private void AddListeners()
        {
            _boughtButton.onClick.AddListener(BoughtClick);
            _sellectButton.onClick.AddListener(SelectClick);
            EventManager.successsBought += SuccessBoughtFunc;
            EventManager.successsSelected += SuccessSelectedFunc;
        }

        private void SuccessBoughtFunc(int id)
        {
            if(_id==id)
            {
                CheckBought(true,false);
            }
        }

        private void SuccessSelectedFunc(int id)
        {
            if (_id == id)
            {
                CheckBought(true, true);
            }
        }

        public Sprite GetSprite()
        {
            return gameObject.GetComponent<Sprite>();
        }

        public void ActiveSelectButton()
        {
            if(_isBought)
            {
                _sellectButton.gameObject.SetActive(true);
            }
        }

        private void BoughtClick()
        {
            EventManager.boughtPlane?.Invoke(_id);
        }

        private void SelectClick()
        {
            CheckBought(true, true);
            EventManager.selectPlane?.Invoke(_id);
        }

        public void UseSetings(int id, int price, bool isBought, bool isCurrentPlane)
        {
            _id = id;
            _price = price;
            RefreshData();
            CheckBought(isBought, isCurrentPlane);
        }

        private void RefreshData()
        {
            _priceText.text = _price.ToString(); ;
        }

        public void CheckBought(bool isBought, bool currentPlane)
        {
            _isBought = isBought;
            _isSelected = currentPlane;
            if (currentPlane)
            {
                _boughtButton.gameObject.SetActive(false);
                _sellectButton.gameObject.SetActive(false);
            }
            else if (isBought)
            {
                _boughtButton.gameObject.SetActive(false);
                _sellectButton.gameObject.SetActive(true);
            }
            else
            {
                _boughtButton.gameObject.SetActive(true);
                _sellectButton.gameObject.SetActive(false);
            }
        }

        public int GetId()
        {
            return _id;
        }

        public int GetPrice()
        {
            return _price;
        }

        private void OnDestroy()
        {
            EventManager.successsBought -= SuccessBoughtFunc;
            EventManager.successsSelected -= SuccessSelectedFunc;
        }
    }
}